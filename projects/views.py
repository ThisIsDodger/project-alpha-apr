from django.shortcuts import render, get_object_or_404, redirect
from projects.models import Project
from projects.forms import ProjectForm
from django.contrib.auth.decorators import login_required


@login_required
def list_projects(request):
    list_projects = Project.objects.filter(owner=request.user)

    context = {
        "list_projects": list_projects,
    }
    return render(request, "projects/list.html", context)


@login_required
def show_project(request, id):
    show_project = get_object_or_404(Project, id=id)
    context = {
        "show_project": show_project,
        "show_project_items": show_project.tasks.all(),
    }
    return render(request, "projects/details.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            task = form.save(False)
            task.author = request.user
            task.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()

        context = {
            "form": form,
        }

        return render(request, "projects/create.html", context)
